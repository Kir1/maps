<?php
ob_start();
echo "<!DOCTYPE HTML>\n";
include_once 'functions/main.php';
ob_end_flush();
access_admin_with_html();

$title = 'Основная информация';

include_once 'functions/form_errors.php';
?>
<?php include_once 'views/main/header.php'; ?>

<body>
<?php include_once 'views/main/main.php'; ?>

<div class = "mainer container">
    <h1><?= $title ?></h1>
    <form action="/controllers/forms.php" class="formMain form-container"  data-ajax-form= 'inner'>
        <input maxlength="60" placeholder="Введите название компании" class="form-inp form-pass" type="text" name="title" value = "<?=  $name_company ?>">
        <input maxlength="11" placeholder="Введите номер телефона" class="form-inp form-pass" type="text" name="phone" value = "<?= $main_phone ?>">
        <textarea rows="2" maxlength="90" placeholder="Введите описание компании" class="form-inp form-pass" type="text" name="description" ><?= $description ?></textarea>
        <input class = "form-valid" type="hidden"  name="valid" value="">
        <input class = "" type="hidden"  name="command" value="save_main_informathion">
        <input class ="button butt-post" type="submit" value="Сохранить">
    </form>
</div>

<?php include_once 'views/main/footer.php'; ?>
</body>
</html>