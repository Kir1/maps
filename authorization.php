<?php
ob_start();
echo "<!DOCTYPE HTML>\n";
include_once 'functions/main.php';
ob_end_flush();
access_admin_with_html(true);

$title = 'Авторизация';

include_once 'functions/form_errors.php';
?>
  <?php include_once 'views/main/header.php'; ?>

  <body>
     <?php include_once 'views/main/main.php'; ?>

     <div class = "mainer container">
         <h1>Авторизация</h1>
         <div class = "ajax-container form-container" data-ajax-container = 'inner' >
             <?php include_once 'views/forms/authorization.php'; ?>
         </div>
     </div>

     <?php include_once 'views/main/footer.php'; ?>
  </body>
</html>