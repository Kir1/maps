<?php
include_once '../functions/main.php';
include_once '../functions/form_errors.php';
$command = get_request_variable('command');

if (  $command == 'auth' )
{
    $errs = rules_errors_form('auth');

    if ( $errs == false ) {
        $_SESSION['admin_password'] = $GLOBALS['admin_pass'];
        echo create_url('admin');
    }
    else {
        $ajax = true;
        include_once '../views/forms/authorization.php';
    }
}
elseif (  $command == 'map_branch_and_offices' ) //вернёт карту и офисы филиала
{
        $bd = bd();
        $officesId = [];
         $offices = [];
        $idBranch = get_request_variable('idBranch');

        if ( $result = $bd->query("SELECT id_office FROM office_branch_rel WHERE id_branch=$idBranch", MYSQLI_USE_RESULT) ) {

            while ($row = $result->fetch_assoc()) {
                $officesId [] = $row['id_office'];
            }

            foreach ($officesId as $idOffice) {
                $idOffice = $idOffice;
                $result2 = $bd->query("SELECT * FROM office WHERE id=$idOffice ORDER BY id", MYSQLI_USE_RESULT);
                while ($row2 = $result2->fetch_assoc()) {

                    $offices [] = $row2;
                };
            };

            $bd->close();
            include_once '../views/forms/map_and_offices.php';
        }

        if ( count($offices) == 0 ) echo  '<b>У данного филиала нет офисов.<b>';
}
elseif (  $command == 'save_main_informathion' ) //создасть заказ при успехе. вернёт вебформу с результатом успешности
{
    if ( access_admin() ) {

        $bd = bd();
        $description = get_request_variable('description');
        $name_company = get_request_variable('title');
        $global_phone = get_request_variable('phone');
        $query = "UPDATE main_information SET global_phone = '$global_phone', description= '$description', name_company = '$name_company' WHERE id=1";

        $bd->query($query);
        $bd->close();
        echo create_url('');
    }
}
?>