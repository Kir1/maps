-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 07 2018 г., 01:14
-- Версия сервера: 5.7.16
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `zgt`
--

-- --------------------------------------------------------

--
-- Структура таблицы `branch`
--

CREATE TABLE `branch` (
  `id` int(10) UNSIGNED NOT NULL,
  `address` tinytext NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `branch`
--

INSERT INTO `branch` (`id`, `address`, `x`, `y`) VALUES
(1, 'Москва', 55.767154693603516, 37.52363204956055),
(2, 'Новгород', 56.250701904296875, 43.94834899902344),
(3, 'Перьм', 58.01152420043945, 56.256378173828125),
(4, 'Кунгур', 57.336212158203125, 56.97981643676758),
(5, 'Суломай', 61.61823654174805, 91.1950912475586);

-- --------------------------------------------------------

--
-- Структура таблицы `main_information`
--

CREATE TABLE `main_information` (
  `id` int(11) NOT NULL,
  `global_phone` varchar(11) NOT NULL,
  `name_company` varchar(60) NOT NULL,
  `description` varchar(90) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `main_information`
--

INSERT INTO `main_information` (`id`, `global_phone`, `name_company`, `description`) VALUES
(1, '89218274799', 'vcxvcv5', 'Джунгарский рай - лучший заповедник для джунгарских хомяков.');

-- --------------------------------------------------------

--
-- Структура таблицы `office`
--

CREATE TABLE `office` (
  `id` int(11) NOT NULL,
  `address` tinytext NOT NULL,
  `metro` tinytext NOT NULL,
  `phones` tinytext NOT NULL,
  `time` tinytext NOT NULL,
  `email` varchar(11) NOT NULL,
  `x` double NOT NULL,
  `y` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `office`
--

INSERT INTO `office` (`id`, `address`, `metro`, `phones`, `time`, `email`, `x`, `y`) VALUES
(1, 'Студенческая 1', 'Студенческое', '<strong>8-985-966-03-06</strong>,<br>\n<strong>8-985-966-03-07</strong>', '<strong>пн-пт:</strong> 09-00 до 18-00<br>\n<strong>сб:</strong> 10-00 до 15-00<br>\n<strong>вс:</strong> 10-00 до 15-00\n', 'email@m1.ru', 55.732730865478516, 37.5313835144043),
(2, 'Студенческая 2', 'Студенческое', '<strong>8-985-966-04-09</strong>,<br>\n<strong>8-985-967-05-07</strong>', '<strong>пн-пт:</strong> 09-00 до 17-00<br>\n<strong>сб:</strong> 11-00 до 15-00<br>\n<strong>вс:</strong> 11-00 до 15-00', 'email@m2.ru', 55.72732162475586, 37.53180694580078),
(3, 'Сокольники 1', 'Соколиное', '<strong>8-915-966-03-06</strong>,<br>\n<strong>8-915-966-03-07</strong>', '<strong>пн-пт:</strong> 05-00 до 18-00<br>\n<strong>сб:</strong> 11-00 до 15-00<br>\n<strong>вс:</strong> 11-00 до 15-00', '', 55.79510498046875, 37.676239013671875),
(4, 'Сокольники 2', 'Соколиное', '<strong>8-915-966-03-06</strong>', '<strong>пн-пт:</strong> 05-00 до 18-00<br>\n<strong>сб:</strong> 11-00 до 15-00<br>\n<strong>вс:</strong> 11-00 до 15-00', '', 55.79510498046885, 37.70311737060547),
(5, 'Ленинский район', '', '89215264899', 'Круглосуточно', '', 56.291866, 43.927079);

-- --------------------------------------------------------

--
-- Структура таблицы `office_branch_rel`
--

CREATE TABLE `office_branch_rel` (
  `id` int(11) NOT NULL,
  `id_office` int(11) NOT NULL,
  `id_branch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `office_branch_rel`
--

INSERT INTO `office_branch_rel` (`id`, `id_office`, `id_branch`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1),
(5, 5, 2);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `main_information`
--
ALTER TABLE `main_information`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `office`
--
ALTER TABLE `office`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `office_branch_rel`
--
ALTER TABLE `office_branch_rel`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `branch`
--
ALTER TABLE `branch`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `main_information`
--
ALTER TABLE `main_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `office`
--
ALTER TABLE `office`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `office_branch_rel`
--
ALTER TABLE `office_branch_rel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
