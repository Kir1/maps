<?php
function rules_errors_form($nameForm){
    $arr = [];
    switch ( $nameForm ) {
        case 'auth':
            $arr =
                [
                    'admin_password' =>  [
                        'admin_password' => 'Ввведите правильный пароль',
                        'required' => 'Введите пароль',
                    ],
                ];
            break;
        case 'webform':
            $arr =
                [
                    'fio' =>  [
                        'required' => 'Введите ФИО'
                    ],
                    'email' =>  [
                        'email' => 'Введите корректный почтовый адрес',
                        'required' => 'Введите почту'
                    ],
                    'files' =>  [
                        'file' => 'Файлов должно быть не более четёрых и они должны соответсвовать конкретным форматам: txt, png, jpg, pdf, doc, docx,lsx',
                        'required' => 'Загрузите файлы'
                    ],
                ];
            break;
    }

    return get_form_errors($arr );
}
?>