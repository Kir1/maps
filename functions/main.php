<?php
$GLOBALS['request'] =  $_REQUEST;
$GLOBALS['admin_pass'] =  '9MonsterHunterForPCeeeee!';

/////////////////////
function get_request_variable($name, $stub='notStub')
{
    if ( isset( $GLOBALS['request'][$name] ) )
        return $GLOBALS['request'][$name];
    else
        if ($stub != 'notStub') return $stub;

    return  false;
};

function get_or_ini_var_session($name,$iniSessionVal='notValue')
{
    if ( isset( $_SESSION[$name] ) ) return $_SESSION[$name];
    if ($iniSessionVal != 'notValue') {
            $_SESSION[$name] = $iniSessionVal;
            return $iniSessionVal;
    }
    return  false;
};

function get_post_var_str($name)
{
    $var = get_request_variable($name) ;
    if ( $var ) return $var;
    return '';
}

//////////////////////

///////////////////////
function get_domain()
{
    return "http://$_SERVER[HTTP_HOST]";
};

function get_full_url()
{
    return get_domain().$_SERVER[REQUEST_URI];
};

function create_url($link='')
{
    if ($link=='') return get_domain();
    return get_domain().'/'.$link;
};

function link_a_check($link)
{
    $actual_link = get_full_url();
    if ( substr_count(  $actual_link, $link) >0 ) return true;
    return  false;
};
/////////////////

///////////////////
function function_redirect($link='')
{
    $url = create_url("$link");
    header("$url");
};
//////////////

///////////////////
function check_admin_password($str)
{
    return $str == $GLOBALS['admin_pass'];
};
function access_admin()
{
    return check_admin_password( get_or_ini_var_session('admin_password') );
};
function access_admin_with_html($invertBool=false)
{
    $bool = $invertBool == true ? true : false;
    if ( access_admin() == $bool ) {
        $title = 'Доступ запрещён';
        include_once 'views/main/acces_denied.php';
        exit;
    }
};
//////////////////////

/////////////////////////////////////////////////
function get_form_errors( $arr )
{
    $errs = [];

    //повсеместная проверка бота
    if (get_request_variable('valid') == '')
    //цикл ошибок
    foreach($arr as $field => $arr2) {
        $val =  get_post_var_str($field);

        //повсеместное правило пустоты(если есть)
        if ( isset($arr2['required']) && $val  == '' ) {
            $errs[$field][]= $arr2['required'];
            continue;
        }

        //остальные правила
        foreach($arr2 as $rule => $mess) {
            if (
                ( $rule == 'email' && filter_var(  $val, FILTER_VALIDATE_EMAIL)  == false )
                || ( $rule == 'admin_password' && check_admin_password( $val) == false )
            )
            $errs[$field][]= $mess;
        }
    }

    if ( count( $errs)==0 )  return false;
    return  $errs;
}
/////////////////////////////////////////////////

/// /////////////////////////////////////////////////
function get_branches()
{
    $bd = bd();
    $array = [];

    if ( $result = $bd->query("SELECT * FROM branch ORDER BY id", MYSQLI_USE_RESULT)) {
        while ($row = $result->fetch_assoc()) {
            $array [$row['address']]= $row;
        }
        $bd->close();
        return $array;
    }
    return false;
};
/// /////////////////////////////////////////////////

////////////////////////////////////BD_AND_SESSION////////////////////////////////////////////////////////////////
function bd()
{
    $mysqli = new mysqli("localhost", "root", "", "zgt");

    if (mysqli_connect_errno()) {
        printf("Connect failed: %s\n", mysqli_connect_error());
        exit();
    };

    return $mysqli;
};

session_start();
get_or_ini_var_session('admin_password','');

$bd = bd();
if ( $result = $bd->query("SELECT * FROM main_information", MYSQLI_USE_RESULT)) {
    $result = $result->fetch_array(MYSQLI_ASSOC);
    $title = $result['name_company'];
    $name_company = $result['name_company'];
    $main_phone = $result['global_phone'];
    $description = $result['description'];
    $bd->close();
}
?>

