<?php
ob_start();
echo "<!DOCTYPE HTML>\n";
include_once 'functions/main.php';
ob_end_flush();

get_branches();
?>
  <?php include_once 'views/main/header.php'; ?>

  <body>
     <?php include_once 'views/main/main.php'; ?>

     <div class = "mainer container main_container">
         <h1><?= $title  ?></h1>
		
         <div class = "main_inf main_descr"><?= $description   ?></div>

         <div id="map" class= "main-map map"></div>
         <div class = "main_inf  main_phone">Основной номер телефона: <?= $main_phone  ?></div>
		 		 
		 <?php
		 $link = 'authorization';
		 $text = 'Вход в административную панель';
		 if ( access_admin()  ) {
			$link = 'admin';
			$text = 'Редактировать основной текст';
		 };
		 ?>
		 <a title = "" href = "/<?= $link  ?>"><?=  $text  ?></a>

         <h2>Филиалы и офисы</h2>
         <div class = "branches">
          <?php
          $branches = get_branches();
          include_once 'views/forms/branches.php';
          ?>
         </div>
     </div>

     <?php include_once 'views/main/footer.php'; ?>
  </body>
</html>