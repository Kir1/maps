$( window ).load(function() {
    window.container_width = $('.main_container').width();

    $('.form-phone').livequery(function(event){
        $(this).mask("+7(999)999-99-99");
    });

    $(".butt-post").livequery(function(event){
        $(this).click(function(ev) {
            ev.preventDefault();
            f_post( $(this).parent() );
        });
    });

    function f_post( formObj, url ) {
        $('#preloader').css('display','flex');

        var fd = new FormData(  $(formObj)[0] );
        $.ajax({
            url:  $(formObj).attr('action'),
            data:fd,
            processData: false,
            contentType: false,
            type: 'POST',
            success: function (data) {
                console.log( data );
                //редирект, если пришла чистая ссылка
                if ( $.trim(data).substr(0,4) == 'http' ) window.location.replace(data);
                //иначе выводим содержимое
                else {
                    var ajax_container = $("[data-ajax-container=" + $(formObj).attr('data-ajax-form') + "]");
                    $(ajax_container).html(data);
                    $('#preloader').hide();

                }
            }
        });
    }

    ymaps.ready(  function() {
        $('.main-map').main_map();
        $('body').branch_map('.branch-map');
        $('body').office_map('.office-map');
    });

    $('.arrow-branch').click(function(ev) {
        var butt =  $(this).parent().find('.butt-post').click();
        $(butt).click();
        $(butt).removeClass('butt-post');
    });
});