(function( $ ){
    $.fn.main_map = function() {
            //////
            var $this = $(this);
            /////////
             $this.map = new ymaps.Map(  $this.attr('id'), {
                 center: [59.93772, 30.313622],
                 zoom: 1,
                 controls: ['fullscreenControl']
             });

             $this.map.behaviors.disable('scrollZoom');

             $this.map.controls.add("zoomControl", {
                position: {top: 15, left: 15}
             });
             ///////////////////////////////////////////////////////////

             ///////////////////////////////////////////////////////////
            $this.branches = new ymaps.GeoObjectCollection({}, {
                preset: "islands#blueHomeCircleIcon",
            });
           var placemarket;
            $( ".branch" ).each(function() {
                var thisObj =  $(this);
                placemarket = new ymaps.Placemark([
                        $(this).attr('data-x'),
                        $(this).attr('data-y')
                    ],
                    {
                        hintContent:   $(this).attr('data-address'),
                        iconCaption: $(this).attr('data-address')
                    }
                );

                placemarket.events.add('click', function () {
                    $('html, body').animate({ scrollTop: $(thisObj).offset().top-20 }, 500);
                });

                $this.branches.add(placemarket);
            });

             $this.map.geoObjects.add( $this.branches);

             $this.map.setBounds( $this.branches.getBounds());

             $( window ).resize(function() {
                 if ( $('.main_container').width() != window.current_size ) {
                     window.current_size = $('.main_container').width();
                     $this.map.setBounds( $this.branches.getBounds());
                 }
             });
             ///////////////////////////////////////////////////////
    };



    $.fn.branch_map = function( nameClass ) {
        $( nameClass ).livequery(function(event){
            var $this = $(this);
            var idBranch = $($this).attr('data-id-branch-map');
            $this.zoom = 3;
            $this.branch_obj = $("[data-id-branch=" + idBranch + "]");
            var centerBranch = [ $(   $this.branch_obj ).attr('data-x'),  $(   $this.branch_obj ).attr('data-y') ];
            /////////
            $this.map = new ymaps.Map(  $this.attr('id'), {
                center:  centerBranch,
                zoom:  $this.zoom,
                controls: ['fullscreenControl']
            });

            $this.map.behaviors.disable('scrollZoom');

            $this.map.controls.add("zoomControl", {
                position: {top: 15, left: 15}
            });
            ///////////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////
            $this.branch = new ymaps.Placemark(
                [
                    $(  $this.branch_obj ).attr('data-x'),
                    $(  $this.branch_obj ).attr('data-y'),

                ],
                {
                    hintContent:   $(  $this.branch_obj ).attr('data-address'),
                    iconCaption: $(  $this.branch_obj ).attr('data-address'),


                }, {
                    preset: "islands#blueHomeIcon",
                }
            );
            $this.map.geoObjects.add( $this.branch );
            ///////////////////////////////////////////////////////

            var offices_branch =   $( ".offices-branch"+ idBranch );
            if ( $( offices_branch ).length == 0 ) return false;



            ///////////////////////////////////////////////////////////
            $this.offices = new ymaps.GeoObjectCollection();
            var placemarket;
            $( offices_branch ).each(function() {
                var thisObj =  $(this);
                placemarket = new ymaps.Placemark([
                        $(this).attr('data-x'),
                        $(this).attr('data-y')
                    ],
                    {
                        hintContent:   $(this).attr('data-address'),
                        iconCaption: $(this).attr('data-address')
                    }
                );

                placemarket.events.add('click', function () {
                    $('html, body').animate({ scrollTop: $(thisObj).parent().offset().top-20 }, 500);
                });

                $this.offices.add(placemarket);
            });

            $this.map.geoObjects.add( $this.offices);
            ///////////////////////////////////////////////////////


            ///////////////////////////////////////////////////////////
            $this.buttOffice = new ymaps.control.Button("Офисы");
            $this.buttBranch = new ymaps.control.Button("Филиал");

            $this.map.controls.add(  $this.buttBranch, {float: 'right'});
            $this.buttBranch.events.add('click', function () {
                $this.buttOffice.deselect();
                $this.offices.options.set('visible', false);
                $this.branch.options.set('visible', true);
                $this.map.setCenter(centerBranch, $this.zoom, {
                    checkZoomRange: true
                });
            });
            $this.buttBranch.select();
            $this.offices.options.set('visible', false);

            $this.map.controls.add(   $this.buttOffice, {float: 'right'});
            $this.buttOffice.events.add('click', function () {
                $this.buttBranch.deselect();
                //$this.buttOffice.select();
                $this.offices.options.set('visible', true);
                $this.branch.options.set('visible', false);

                if ( $( offices_branch ).length == 1 ) {

                    $this.map.setCenter(
                        [ $( offices_branch[0] ).attr('data-x') , $( offices_branch[0] ).attr('data-y')]
                        , 15, {
                        checkZoomRange: true
                    });
                }
                else

                $this.map.setBounds( $this.offices.getBounds());
            });
            ///////////////////////////////////////////////////////////
        });
    }



    $.fn.office_map = function( nameClass ) {
        $( nameClass ).livequery(function(event){
            var $this = $(this);

            /////////
            $this.map = new ymaps.Map(  $this.attr('id'), {
                center: [ $( $this ).attr('data-x'),  $( $this ).attr('data-y') ],
                zoom: 15,
                controls: ['fullscreenControl']
            });
            $this.map.behaviors.disable('scrollZoom');

            $this.map.controls.add("zoomControl", {
                position: {top: 15, left: 15}
            });
            ///////////////////////////////////////////////////////////

            ///////////////////////////////////////////////////////////
            var placemark = new ymaps.Placemark(
                [
                    $( $this ).attr('data-x'),
                    $( $this ).attr('data-y')
                ],
                {
                    hintContent:   $(this).attr('data-address'),
                    iconCaption: $(this).attr('data-address')
                }
            );
            $this.map.geoObjects.add(placemark);
            ///////////////////////////////////////////////////////
        });
    }
})( jQuery );