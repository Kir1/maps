<?php
if ( isset($branches) ) foreach($branches as $branch) {
    $containerName = 'branch_content'.$branch['id'];
?>
    <div class = "branch-container template1">

        <div class = "branch"
             data-x = '<?= $branch['x'] ?>'
             data-y = '<?= $branch['y']?>'
             data-address = '<?= $branch['address'] ?>'
             data-id-branch = '<?= $branch['id'] ?>'
        >

            <i class="fa fa-sitemap ik_none_mobile" aria-hidden="true"></i>

            <div class = "adress_branch"><?= $branch['address'] ?></div>

            <i class="fa fa-arrow-down pointer arrow-branch" aria-hidden="true"></i>

            <form action="/controllers/forms.php" class="formMain hideForm"  data-ajax-form= "<?= $containerName ?>" >
                <input class = "form-valid" type="hidden"  name="valid" value="">
                <input class = "" type="hidden"  name="command" value="map_branch_and_offices">
                <input class = "" type="hidden"  name="idBranch" value="<?= $branch['id'] ?>">
                <input class ="button butt-post" type="submit" value="">
            </form>
        </div>

        <div class = "branch-content" data-ajax-container= "<?= $containerName ?>" >
        </div>
    </div>
<?php
}
?>

