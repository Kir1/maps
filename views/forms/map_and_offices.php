<div class = "map_and_offices">

<div id="branch-map<?= $idBranch ?>" class= "branch-map map"
     data-id-branch-map = '<?= $idBranch ?>'
></div>

<div class = "offices">
    <?php
    if ( isset( $offices) ) foreach( $offices as $office) {
        ?>

        <div class = "office">
            <div class = "office-main">

                <?php
                $valOffice = $office['address'];
                $ikonOffice = 'fa-university';
                $fieldOffice = 'Адрес';
                include $_SERVER['DOCUMENT_ROOT'].'/views/forms/office_block.php';
                ?>

                <?php
                $valOffice = $office['metro'];
                $ikonOffice = 'fa-maxcdn';
                $fieldOffice = 'Метро';
                include $_SERVER['DOCUMENT_ROOT'].'/views/forms/office_block.php';
                ?>

                <?php
                $valOffice = $office['phones'];
                $ikonOffice = 'fa-phone';
                $fieldOffice = 'Телефоны';
                include $_SERVER['DOCUMENT_ROOT'].'/views/forms/office_block.php';
                ?>

                <?php
                $valOffice = $office['time'];
                $ikonOffice = 'fa-clock-o';
                $fieldOffice = 'Время работы';
                include $_SERVER['DOCUMENT_ROOT'].'/views/forms/office_block.php';
                ?>

                <?php
                $valOffice = $office['email'];
                $ikonOffice = 'fa-envelope-open-o';
                $fieldOffice = 'Почта';
                include $_SERVER['DOCUMENT_ROOT'].'/views/forms/office_block.php';
                ?>
            </div>

            <div id="office-map<?= $office['id'] ?>"
                 data-x = '<?= $office['x'] ?>'
                 data-y = '<?= $office['y']?>'
                 data-address = '<?= $office['address'] ?>'
                 class= "office-map map offices-branch<?= $idBranch?>">
            </div>

        </div>
        <?php
    }
    ?>
</div>

</div>
