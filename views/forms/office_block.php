<div class = "office-inf-block">
    <?php
    if ( $valOffice != '' ) {
        ?>
        <i class="fa <?= $ikonOffice ?>" aria-hidden="true"></i>
        <b><?= $fieldOffice ?></b>: <br> <?= $valOffice ?>
        <?php
    }
    ?>
</div>