<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php if ( isset( $title) ) echo $title; else echo 'Ошибка 404'; ?></title>
    <meta name="description" content="<?= $description ?>">
    <link href="/css/font-awesome.min.css" rel="stylesheet">
    <link href="/css/style.css" rel="stylesheet">
</head>